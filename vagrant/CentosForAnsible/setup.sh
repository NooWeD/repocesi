#!/bin/bash
sudo su -
# Mise à jour du système
yum update -y

# Installation d'un paquet vim
yum install -y vim

# Installation d'un paquet python3
yum install -y python3

# Installation d'epel 
yum install epel-release

# Installation d'Ansible
yum install ansible
history -c